<?php $software_name = "PBchat";$software_version = "1.4";$software_info = "Simple Chat Application";$software_website = "www.pb-soft.com";$software_company = "PB-Soft";if(isset($_SERVER['SERVER_NAME'])&&isset($_SERVER['SERVER_PORT'])){define("DOMAIN", $_SERVER['SERVER_NAME'].":".$_SERVER['SERVER_PORT']);}else{define("DOMAIN", $_SERVER['HTTP_HOST']);}$path_parts = pathinfo($_SERVER["SCRIPT_NAME"]);define("SCRIPTDIR", DOMAIN.$path_parts['dirname']."/");$config_file = "config/config.php";include($config_file);echo"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";echo"<html xmlns=\"http://www.w3.org/1999/xhtml\">\n";echo"<head>\n";echo"<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\" />\n";echo"<title>";echo $software_name." ".$software_version." - ".$software_info;echo"</title>\n";echo"<meta name=\"author\" content=\"Patrick Biegel, PB-Soft\" />\n";echo"<meta name=\"description\" content=\"PBchat is a simple chat application. Check out our website at www.pb-soft.com for more information !\" />\n";echo"<meta name=\"keywords\" content=\"pb-soft, pb-soft.com, pbchat, web, application, applications, chat, communicate, script, scripts, php\" />\n";echo"<link rel=\"stylesheet\" type=\"text/css\" href=\"css/pbchat.css\" />\n"; ?>


 <script type="text/javascript">


 //===================================================================
 // Initialize variables.
 //===================================================================
 var software_name;
 var use_password;
 var name_padding;
 var info_message;
 var script_dir;
 var saved_pass;
 var show_date;


 //===================================================================
 // Get variable values from php.
 //===================================================================
 software_name = "<?php echo $software_name; ?>";
 use_password = <?php echo $use_password; ?>;
 name_padding = <?php echo $name_padding; ?>;
 info_message = <?php echo $info_message; ?>;
 script_dir = "<?php echo SCRIPTDIR; ?>";
 saved_pass = "<?php echo $password; ?>";
 show_date = <?php echo $show_date; ?>;


 </script>


<?php echo"<script src=\"js/pbchat.js\" type=\"text/javascript\"></script>\n";echo"</head>\n";echo"<body>\n";echo"<div class=\"center\">\n";if($display_title==1){echo"<div class=\"chat-title\">\n";echo $chat_title."\n";echo"</div>\n";}echo"<div class=\"tools\">\n";echo"Username: \n";echo"<input class=\"name\" name=\"name\" id=\"name\" type=\"text\" size=\"10\" maxlength=\"".($name_padding - 2)."\" />\n";echo"&nbsp;&nbsp;\n";if($use_password==1){echo"Password: \n";echo"<input class=\"password\" name=\"password\" id=\"password\" type=\"password\" size=\"8\" maxlength=\"20\" />\n";echo"<span id=\"status\"></span>\n";echo"&nbsp;&nbsp;\n";}echo"Refresh: \n";echo"<input class=\"time\" name=\"time\" id=\"time\" type=\"text\" size=\"2\" maxlength=\"3\" value=\"".$default_refresh."\" />\n";echo"&nbsp;&nbsp;\n";echo"Remaining: \n";echo"<input class=\"countdown\" name=\"countdown\" id=\"countdown\" type=\"text\" size=\"2\" maxlength=\"3\" readonly=\"readonly\" value=\"".$default_refresh."\" />\n";echo"&nbsp;&nbsp;\n";echo"</div>\n";echo"<textarea class=\"output\" name=\"output\" id=\"output\" cols=\"10\" rows=\"1\" readonly=\"readonly\"></textarea>\n";echo"<br />\n";echo"<textarea class=\"input\" name=\"input\" id=\"input\" cols=\"10\" rows=\"1\" onkeydown=\"getkey(event);\"></textarea>\n";echo"<br />\n";echo"<span class=\"button\">\n";echo"<a href=\"http://".SCRIPTDIR.$logfile."\" onclick=\"window.open(this.href, 'Logfile').focus(); return false;\">";echo"Show Logfile";echo"</a>\n";echo"</span>\n";echo"&nbsp;&nbsp;\n";if($delete_link==1){$admin_flag = 0;$ip_parts = explode(".", $_SERVER["REMOTE_ADDR"]);if(in_array($ip_parts[0], $admin_ip)){$admin_flag = 1;}elseif(in_array($ip_parts[0].".".$ip_parts[1], $admin_ip)){$admin_flag = 1;}elseif(in_array($ip_parts[0].".".$ip_parts[1].".".$ip_parts[2], $admin_ip)){$admin_flag = 1;}elseif(in_array($ip_parts[0].".".$ip_parts[1].".".$ip_parts[2].".".$ip_parts[3], $admin_ip)){$admin_flag = 1;}if($admin_flag==1){echo"<span class=\"button\">\n";echo"<a onclick=\"delete_log()\">";echo"Delete Logfile";echo"</a>\n";echo"</span>\n";echo"&nbsp;&nbsp;\n";}}if($use_password==1){echo"<span class=\"button\">\n";echo"<a onclick=\"document.getElementById('password').value = '';\">";echo"Reset Password";echo"</a>\n";echo"</span>\n";echo"&nbsp;&nbsp;\n";}echo"<span class=\"button\">\n";echo"<a onclick=\"document.getElementById('input').value = '';\">";echo"Reset Input";echo"</a>\n";echo"</span>\n";echo"&nbsp;&nbsp;\n";echo"<span class=\"button\">\n";echo"<a onclick=\"document.getElementById('output').value = '';\">";echo"Reset Output";echo"</a>\n";echo"</span>\n";echo"&nbsp;&nbsp;\n";echo"<span class=\"button\">\n";echo"<a onclick=\"send_input()\">";echo"Send Message";echo"</a>\n";echo"</span>\n";echo"<div class=\"copyright\">\n";echo"Copyright &copy; ".date("Y")." by <a class=\"copyright\" href=\"http://".$software_website."\" onclick=\"window.open(this.href, 'Logfile').focus(); return false;\">".$software_company."</a>\n";echo" | IP address: ".$_SERVER["REMOTE_ADDR"];echo"</div>\n";echo"</div>\n"; ?>


<script type="text/javascript">


 //===================================================================
 // Display the start information message.
 //===================================================================
 display_info();


 //===================================================================
 // Start the countdown.
 //===================================================================
 countdown();


 //===================================================================
 // Start the chat actualization.
 //===================================================================
 actualize_chat();


</script>


<?php echo"</body>\n";echo"</html>\n"; 