<?php


// =====================================================================
// Specify the default timezone.
// =====================================================================
//
// If the timezone is not defined in your PHP configuration file, you
// can specify it with the following setting:
//
//  date_default_timezone_set('UTC');
//
// You have to uncomment the setting and enter your preferred timezone.
// For a list of available timezones check the following link:
//
//   http://www.php.net/manual/en/timezones.php
//
// Normally it is easier to specify the default timezone in the PHP
// configuration file 'php.ini'. You have to add the following lines to
// the configuration file:
//
//   [Date]
//   date.timezone = UTC
//
// You then can replace 'UTC' with your own timezone.
//
// =====================================================================
// date_default_timezone_set('America/La_Paz');
date_default_timezone_set('Europe/Zurich');


// =====================================================================
// Please specify if the chat title is displayed.
// =====================================================================
$display_title = 1;


// =====================================================================
// Please specify the chat title.
// =====================================================================
$chat_title = "PBchat - Simple Chat Application";


// =====================================================================
// Please specify the padding length of the user name.
// =====================================================================
$name_padding = 16;


// =====================================================================
// Please specify the name of the logfile.
// =====================================================================
$logfile = "log/pbchat.log";


// =====================================================================
// Please specify if the date and time should be displayed.
// =====================================================================
$show_date = 1;


// =====================================================================
// Please specify if a password is used to access the chat.
// =====================================================================
$use_password = 1;


// =====================================================================
// Please specify the used password as a MD5 hash.
// =====================================================================
$password = "d6a5c9544eca9b5ce2266d1c34a93222";  // Password: firefox


// =====================================================================
// Please specify if the link to delete the logfile should be displayed.
// =====================================================================
$delete_link = 1;


// =====================================================================
// Please specify the IP address which can delete the logfile. You can
// also specify only a part of the IP address:
//
//  186.27.1.10  Matches the ip address   186.27.1.10
//  186.27.1     Matches the ip addresses 186.27.1.x
//  186.27       Matches the ip addresses 186.27.x.x
//  186          Matches the ip addresses 186.x.x.x
//
// =====================================================================
$admin_ip[] = "192";


// =====================================================================
// Please specify if the information message should be displayed. This
// window asks the user at the start to enter his username.
// =====================================================================
$info_message = 1;


// =====================================================================
// Please specify the default refresh time.
// =====================================================================
$default_refresh = 10;
