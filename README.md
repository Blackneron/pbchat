# PBchat - README #
---

### Overview ###

The **PBchat** tool is a small and simple chat application without any additional libraries needed. Everything is generated with PHP, HTML and CSS code. The whole application is only about 94 KB in size. If you minimize the files the size is reduzed to about 22 KB.

### Screenshot ###

![PBchat - Chat Window](development/readme/pbchat.png "PBchat - Chat Window")

### Setup ###

* Copy the whole directory **pbchat** to your webhost.
* Edit the configuration file **pbchat/config/config.php**.
* Open the file **pbchat/pbchat.php** to see start chatting.
* Enter first your username and the default password **firefox**.
* Then you can begin to write comments.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **PBchat** tool is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
