<?php


// =====================================================================
// Display the chat output.
// =====================================================================
echo send_output(trim($_REQUEST['line']));


// =====================================================================
// Function which sends the chat output.
// =====================================================================
function send_output($start_line) {


  // ===================================================================
  // Initialize variables.
  // ===================================================================
  $error_message = "";
  $output_text = "";


  // ===================================================================
  // Specify the configuration file.
  // ===================================================================
  $config_file = "config/config.php";


  // ===================================================================
  // Check if the configuration file exist.
  // ===================================================================
  if (file_exists($config_file)) {


    // =================================================================
    // Include the configuration file.
    // =================================================================
    include($config_file);


    // =================================================================
    // Check if the logfile exist.
    // =================================================================
    if (isset($logfile) && !file_exists($logfile)) {


      // ===============================================================
      // Create the logfile.
      // ===============================================================
      $handle = fopen($logfile, 'a');


      // ===============================================================
      // Check if the logfile could be created (handle available).
      // ===============================================================
      if ($handle) {


        // =============================================================
        // Close the logfile.
        // =============================================================
        fclose($handle);


        // =============================================================
        // The logfile could not be created.
        // =============================================================
      } else {


        // =============================================================
        // Specify the error message.
        // =============================================================
        $error_message = "Could not create the logfile !";

      } // The logfile could not be created.

    } // Check if the logfile exist.


    // =================================================================
    // The configuration file does not exist.
    // =================================================================
  } else {


    // =================================================================
    // Add an error message to the error array.
    // =================================================================
    $error_message = "The configuration file does not exist !";

  } // The configuration file does not exist.


  // ===================================================================
  // Check if there was an error.
  // ===================================================================
  if (isset($name_padding) && $error_message != "") {


    // =================================================================
    // Pad the error message info.
    // =================================================================
    $error_info = str_pad("[Error]", $name_padding, " ");


    // =================================================================
    // Specify the status message.
    // =================================================================
    $output_text = $error_info.$error_message."\n";


    // =================================================================
    // There was no error.
    // =================================================================
  } elseif (isset($logfile)) {


    // =================================================================
    // Get the lines of the logfile into an array.
    // =================================================================
    $log_lines = file($logfile);


    // =================================================================
    // Loop through the log_lines array.
    // =================================================================
    for ($line_counter = $start_line; $line_counter < count($log_lines); $line_counter++) {


      // ===============================================================
      // Increase the start line variable.
      // ===============================================================
      $line_number = $line_counter + 1;


      // ===============================================================
      // Pad the actual line number.
      // ===============================================================
      $line_number = str_pad($line_number, 3, " ", STR_PAD_LEFT);


      // ===============================================================
      // Add the actual line to the output.
      // ===============================================================
      $output_text .= $line_number." ".$log_lines[$line_counter];

    } // Loop through the log_lines array.

  } // There was no error.


  // ===================================================================
  // Return the status text.
  // ===================================================================
  return $output_text;

} // Function which sends the chat output.
