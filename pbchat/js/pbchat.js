// #####################################################################
// #####################################################################
// ############   J S L I N T   C O N F I G U R A T I O N   ############
// #####################################################################
// #####################################################################

// Specify the JSlint options (for-loops and 'this' can be used and the
// strict whitespaces rules are disabled and also the bitwise operators
// are tolerated).
/*jslint
  for, this, white, bitwise
*/

// Specify the global variables which JSlint can not see.
/*global
  XMLHttpRequest, ActiveXObject, alert, navigator, software_name,
  use_password, name_padding, info_message, script_dir, saved_pass,
  show_date, document, window, setTimeout, md5, unescape, define
*/


// =====================================================================
// Function to display the start information message.
// =====================================================================
function display_info() {


  // ===================================================================
  // Enable the strict mode of JSlint.
  // ===================================================================
  "use strict";


  // ===================================================================
  // Display the information message.
  // ===================================================================
  if(info_message === 1) {


    // =================================================================
    // Check if the password security is used.
    // =================================================================
    if(use_password === 1) {


      // ===============================================================
      // Display an error message.
      // ===============================================================
      alert("Welcome to " + software_name + " !\n\nPlease enter your username and password first !");


      // ===============================================================
      // The password security is not used.
      // ===============================================================
    } else {


      // ===============================================================
      // Display an error message.
      // ===============================================================
      alert("Welcome to " + software_name + " !\n\nPlease enter your username first !");

    } // The password security is not used.

  } // Display the information message.

} // Function to display the start information message.


// =====================================================================
// Function to actualize the chat.
// =====================================================================
function actualize_chat() {


  // ===================================================================
  // Enable the strict mode of JSlint.
  // ===================================================================
  "use strict";


  // ===================================================================
  // Variable declarations.
  // ===================================================================
  var refresh_time;
  var time;


  // ===================================================================
  // Update the chat output.
  // ===================================================================
  // noinspection NestedFunctionCallJS
  get_output();


  // ===================================================================
  // Get the time from the input field.
  // ===================================================================
  time = document.getElementById('time').value;


  // ===================================================================
  // Check if the time is not empty.
  // ===================================================================
  if(time !== "") {


    // =================================================================
    // Check if the input is not a number.
    // =================================================================
    if(isNaN(time)) {


      // ===============================================================
      // Specify the default refresh time.
      // ===============================================================
      refresh_time = 20000;


      // ===============================================================
      // Display the minimum refresh time.
      // ===============================================================
      document.getElementById('time').value = 20;


      // ===============================================================
      // Display an error message.
      // ===============================================================
      alert("Please enter a valid refresh time !");


      // ===============================================================
      // The input is a number.
      // ===============================================================
    } else {


      // ===============================================================
      // Check if the time is < 10 seconds.
      // ===============================================================
      if(time < 10) {


        // =============================================================
        // Specify the minimum refresh time.
        // =============================================================
        refresh_time = 10000;


        // =============================================================
        // Display the minimum refresh time.
        // =============================================================
        document.getElementById('time').value = 10;


        // =============================================================
        // The time is >= 5 seconds.
        // =============================================================
      } else {


        // =============================================================
        // Specify the refresh time.
        // =============================================================
        refresh_time = time * 1000;

      } // The time is >= 5 seconds.

    } // The input is a number.


    // =================================================================
    // No refresh time was specified.
    // =================================================================
  } else {


    // =================================================================
    // Specify the default refresh time.
    // =================================================================
    refresh_time = 20000;


    // =================================================================
    // Display the minimum refresh time.
    // =================================================================
    document.getElementById('time').value = 20;


    // =================================================================
    // Display an error message.
    // =================================================================
    alert("Please enter a refresh time !");

  } // No refresh time was specified.


  // ===================================================================
  // Wait some time before updating the chat information.
  // ===================================================================
  // noinspection NestedFunctionCallJS
  setTimeout("actualize_chat()", refresh_time);

} // Function to actualize the chat.


// =====================================================================
// Function to get the chat updates from the host.
// =====================================================================
function get_output() {


  // ===================================================================
  // Enable the strict mode of JSlint.
  // ===================================================================
  "use strict";


  // ===================================================================
  // Variable declarations.
  // ===================================================================
  var http = false;
  var chat_output;
  var last_line;
  var pattern1;
  var pattern2;
  var status;
  var match1;
  var match2;
  var line;


  // ===================================================================
  // Check if the password security is used.
  // ===================================================================
  if(use_password === 1) {


    // =================================================================
    // Get the password status.
    // =================================================================
    status = check_pass();


    // =================================================================
    // No password security is used.
    // =================================================================
  } else {


    // =================================================================
    // Specify the password status.
    // =================================================================
    status = 1;

  } // No password security is used.


  // ===================================================================
  // Check if the password status is OK.
  // ===================================================================
  if(status === 1) {


    // =================================================================
    // Specify the regex pattern for matching the last output line.
    // =================================================================
    pattern1 = /[0-9]+\ \[[0-9\.]{10}\|[0-9:]{8}\]\[[a-zA-Z\ ]+\][\w\ ]+[\r\n]{0,2}$/;


    // =================================================================
    // Specify the chat output.
    // =================================================================
    chat_output = document.getElementById('output').value;


    // =================================================================
    // Search for the last line with the line number.
    // =================================================================
    match1 = pattern1.exec(chat_output);


    // =================================================================
    // Check if the last line was found.
    // =================================================================
    if (match1 !== null) {


      // ===============================================================
      // Store the last line.
      // ===============================================================
      last_line = match1[0];


      // ===============================================================
      // Specify the regex pattern for matching the last line number.
      // ===============================================================
      pattern2 = /^[0-9]+/;


      // ===============================================================
      // Search for the line number.
      // ===============================================================
      match2 = pattern2.exec(last_line);


      // ===============================================================
      // Check if the line number was found.
      // ===============================================================
      if (match2 !== null) {


        // =============================================================
        // Specify the line number.
        // =============================================================
        line = match2[0];


        // =============================================================
        // There was no line number found.
        // =============================================================
      } else {


        // =============================================================
        // Specify the line number.
        // =============================================================
        line = 0;

      } // There was no line number found.


      // ===============================================================
      // There was no last line found.
      // ===============================================================
    } else {


      // ===============================================================
      // Specify the line number.
      // ===============================================================
      line = 0;

    } // There was no last line found.


    // =================================================================
    // Check if the chat output has to be deleted.
    // =================================================================
    if(line === 0) {


      // ===============================================================
      // Delete the chat output.
      // ===============================================================
      document.getElementById('output').value = '';

    } // Check if the chat output has to be deleted.


    // =================================================================
    // Check if the Internet Explorer is used.
    // =================================================================
    if(navigator.appName === "Microsoft Internet Explorer") {


      // ===============================================================
      // Generate a new XML HTTP Request object - Internet Explorer.
      // ===============================================================
      http = new ActiveXObject("Microsoft.XMLHTTP");


      // ===============================================================
      // No Internet Explorer is used.
      // ===============================================================
    } else {


      // ===============================================================
      // Generate a new XML HTTP Request object - Other browsers.
      // ===============================================================
      http = new XMLHttpRequest();

    } // No Internet Explorer is used.


    // =================================================================
    // Abort existing requests.
    // =================================================================
    http.abort();


    // =================================================================
    // Wait for the answer from the host and check if the readyState
    // attribute changes.
    // =================================================================
    http.onreadystatechange=function() {


      // ===============================================================
      // Check if the readyState attribute is set to 4 what means that
      // the request is finished. Check also if the HTTP status is 200
      // what means that the page was available (404 if the page was
      // not found).
      // ===============================================================
      if(http.readyState === 4 && http.status === 200) {


        // =============================================================
        // Check if there is an output.
        // =============================================================
        if(http.responseText !== '') {


          // ===========================================================
          // Insert the chat output into the element with the ID 'output'.
          // ===========================================================
          document.getElementById('output').value = document.getElementById('output').value + http.responseText;


          // ===========================================================
          // Scroll to the bottom of the content.
          // ===========================================================
          document.getElementById('output').scrollTop = document.getElementById('output').scrollHeight;

        } // Check if there is an output.


        // =============================================================
        // Update the countdown.
        // =============================================================
        document.getElementById('countdown').value = document.getElementById('time').value;

      } // Check if the readyState attribute is set to 4...

    }; // Wait for the answer from the host and check if the readyState attribute changes.


    // =================================================================
    // Open a connection and make a request to the host.
    // =================================================================
    http.open("GET", location.protocol + "//" + script_dir + "chat_output.php?line=" + line, true);
    http.send(null);

  } // Check if the password status is OK.

} // Function to get the chat updates from the host.


// =====================================================================
// Function to send the chat input to the host.
// =====================================================================
function send_input() {


  // ===================================================================
  // Enable the strict mode of JSlint.
  // ===================================================================
  "use strict";


  // ===================================================================
  // Initialize variables.
  // ===================================================================
  var http = false;
  var date_string;
  var name_string;
  var input_text;
  var send_text;
  var username;
  var pattern1;
  var pattern2;
  var match1;
  var match2;
  var status;


  // ===================================================================
  // Check if the password security is used.
  // ===================================================================
  if(use_password === 1) {


    // =================================================================
    // Get the password status.
    // =================================================================
    status = check_pass();

  } // Check if the password security is used.


  // ===================================================================
  // Check if the password status is OK.
  // ===================================================================
  if(status === 1 || use_password === 0) {


    // =================================================================
    // Store the username.
    // =================================================================
    username = document.getElementById('name').value;


    // =================================================================
    // Check if a name was entered.
    // =================================================================
    if(username !== "") {


      // ===============================================================
      // Specify the regex pattern for matching the username.
      // ===============================================================
      pattern1 = new RegExp("^[a-zA-Z0-9_'-]{3," + (name_padding - 2) + "}$");


      // ===============================================================
      // Check the username.
      // ===============================================================
      match1 = pattern1.exec(username);


      // ===============================================================
      // Check if the username is valid.
      // ===============================================================
      if (match1 !== null) {


        // =============================================================
        // Store the text from the input area.
        // =============================================================
        input_text = document.getElementById('input').value;


        // =============================================================
        // Check if a text message was entered.
        // =============================================================
        if(input_text !== "") {


          // ===========================================================
          // Specify the regex pattern for matching the text message.
          // ===========================================================
          pattern2 = /^[\ \r\n\ta-zA-Z0-9_'\|!"#$%&\/()=?¡¿*+,;\.:<>{}£¤¥§ÄÅÆÉÑÖØÜßàäåæçèéìñòöøùü€\-\[\]]+$/;


          // ===========================================================
          // Check the text message.
          // ===========================================================
          match2 = pattern2.exec(input_text);


          // ===========================================================
          // Check if the text message is valid.
          // ===========================================================
          if(match2 !== null) {


            // =========================================================
            // Check if the Internet Explorer is used.
            // =========================================================
            if(navigator.appName === "Microsoft Internet Explorer") {


              // =======================================================
              // Generate a new XML HTTP Request object - Internet
              // Explorer.
              // =======================================================
              http = new ActiveXObject("Microsoft.XMLHTTP");


              // =======================================================
              // No Internet Explorer is used.
              // =======================================================
            } else {


              // =======================================================
              // Generate a new XML HTTP Request object - Other
              // browsers.
              // =======================================================
              http = new XMLHttpRequest();

            } // No Internet Explorer is used.


            // =========================================================
            // Abort existing requests.
            // =========================================================
            http.abort();


            // =========================================================
            // Wait for the answer from the host and check if the
            // readyState attribute changes.
            // =========================================================
            http.onreadystatechange=function() {


              // =======================================================
              // Check if the readyState attribute is set to 4 what
              // means that the request is finished. Check also if the
              // HTTP status is 200 what means that the page was
              // available (404 if the page was not found).
              // =======================================================
              if(http.readyState === 4 && http.status === 200) {


                // =====================================================
                // Check if there is an error message to display.
                // =====================================================
                if(http.responseText !== '[OK]') {


                  // ===================================================
                  // Insert the error message into the output screen.
                  // ===================================================
                  document.getElementById('output').value = http.responseText;

                } // Check if there is an error message to display.


                // =====================================================
                // Clear the input area.
                // =====================================================
                document.getElementById('input').value = '';


                // =====================================================
                // Update the chat output.
                // =====================================================
                get_output();

              } // Check if the readyState attribute is set to 4...

            }; // Wait for the answer from the host and check if the readyState attribute changes.


            // =========================================================
            // Check if the date and time should be displayed.
            // =========================================================
            if(show_date === 1) {


              // =======================================================
              // Create a new date.
              // =======================================================
              var now = new Date();


              // =======================================================
              // Get the day, month and year.
              // =======================================================
              var day = now.getDate();
              var month = now.getMonth();
              var year = now.getFullYear();


              // =======================================================
              // Get the hours, minutes and seconds.
              // =======================================================
              var hours = now.getHours();
              var minutes = now.getMinutes();
              var seconds = now.getSeconds();


              // =======================================================
              // Specify the date string.
              // =======================================================
              date_string = '[' + pad_date(day) + '.' + pad_date(month) + '.' + year + '|' + pad_date(hours) + ':' + pad_date(minutes) + ':' + pad_date(seconds) + ']';


              // =======================================================
              // Specify the text to send.
              // =======================================================
              send_text = date_string;


              // =======================================================
              // The date and time should not be displayed.
              // =======================================================
            } else {

              // =======================================================
              // Initialize the send text.
              // =======================================================
              send_text = '';

            } // The date and time should not be displayed.


            // =========================================================
            // Specify the name string.
            // =========================================================
            name_string = '[' + username + ']';


            // =========================================================
            // Pad the name string.
            // =========================================================
            name_string = pad_string(name_string, name_padding);


            // =========================================================
            // Specify the text to send.
            // =========================================================
            send_text = send_text + name_string + document.getElementById('input').value;


            // =========================================================
            // Open a connection and make a request to the host.
            // =========================================================
            http.open("GET", location.protocol + "//" + script_dir + "chat_input.php?text=" + encodeURIComponent(send_text), true);
            http.send(null);


            // =========================================================
            // The text message is not valid.
            // =========================================================
          } else {


            // =========================================================
            // Display an error message.
            // =========================================================
            alert("Please enter a valid text message !\n\nPlease try again !");

          } // The text message is not valid.


          // ===========================================================
          // No text message was entered.
          // ===========================================================
        } else {


          // ===========================================================
          // Display an error message.
          // ===========================================================
          alert("The 'message' field is empty !\n\nPlease enter a message first !");

        } // No text message was entered.


        // =============================================================
        // The username is not valid.
        // =============================================================
      } else {


        // =============================================================
        // Display an error message.
        // =============================================================
        alert("Please enter a valid username !\n\nThe username has to be between 3 and " + (name_padding - 2) + " characters long.\n\nPlease use only the following characters:\n\n A-Z\n a-z\n 0-9\n _ ' -\n\nPlease try again !");

      } // The username is not valid.


      // ===============================================================
      // No name was entered.
      // ===============================================================
    } else {


      // ===============================================================
      // Display an error message.
      // ===============================================================
      alert("The field 'Username' is empty !\n\nPlease enter your username first !");

    } // No name was entered.

  } // Check if the password status is OK.

} // Function to send the chat input to the host.


// =====================================================================
// Function to check the password.
// =====================================================================
function check_pass() {


  // ===================================================================
  // Enable the strict mode of JSlint.
  // ===================================================================
  "use strict";


  // ===================================================================
  // Initialize variables.
  // ===================================================================
  var input_pass;
  var status;


  // ===================================================================
  // Store the input password hash.
  // ===================================================================
  input_pass = md5(document.getElementById('password').value);


  // ===================================================================
  // Check if the two password hashes match.
  // ===================================================================
  if(input_pass === saved_pass) {


    // =================================================================
    // Display the password status.
    // =================================================================
    document.getElementById('status').innerHTML = "<input class=\"status-ok\" type=\"text\" size=\"3\" maxlength=\"3\" readonly=\"readonly\" value=\"OK\"/>";


    // =================================================================
    // Specify the password status.
    // =================================================================
    status = 1;


    // =================================================================
    // The two password hash do not match.
    // =================================================================
  } else {


    // =================================================================
    // Display the password status.
    // =================================================================
    document.getElementById('status').innerHTML = "<input class=\"status-fail\" type=\"text\" size=\"3\" maxlength=\"3\" readonly=\"readonly\" value=\"BAD\"/>";


    // =================================================================
    // Specify the password status.
    // =================================================================
    status = 0;

  } // The two password hash do not match.


  // ===================================================================
  // Return the password status.
  // ===================================================================
  return status;

} // Function to check the password.


// =====================================================================
// Function to right-pad a string with spaces.
// =====================================================================
function pad_string(text, length) {


  // ===================================================================
  // Enable the strict mode of JSlint.
  // ===================================================================
  "use strict";


  // ===================================================================
  // Initialize variables.
  // ===================================================================
  var output = text;


  // ===================================================================
  // Loop and pad the string.
  // ===================================================================
  while (output.length < length) {


    // =================================================================
    // Pad the string.
    // =================================================================
    output += ' ';

  } // Loop and pad the string.


  // ===================================================================
  // Return the string.
  // ===================================================================
  return output;

} // Function to right-pad a string with spaces.


// =====================================================================
// Function to left-pad a date or time part with a zero.
// =====================================================================
function pad_date(input) {


  // ===================================================================
  // Enable the strict mode of JSlint.
  // ===================================================================
  "use strict";


  // ===================================================================
  // Initialize variables.
  // ===================================================================
  var output = input.toString();


  // ===================================================================
  // Check if the string has to be padded.
  // ===================================================================
  if(output.length < 2) {


    // =================================================================
    // Pad the string.
    // =================================================================
    output = '0' + output;

  } // Check if the string has to be padded.


  // ===================================================================
  // Return the string.
  // ===================================================================
  return output;

} // Function to left-pad a date or time part with a zero.


// =====================================================================
// Function to countdown the refresh timer.
// =====================================================================
function countdown() {


  // ===================================================================
  // Enable the strict mode of JSlint.
  // ===================================================================
  "use strict";


  // ===================================================================
  // Variable declarations.
  // ===================================================================
  var new_value;
  var status;


  // ===================================================================
  // Check if the password security is used and the status is zero.
  // ===================================================================
  if(use_password === 1 && status === 0) {


    // =================================================================
    // Get the password status.
    // =================================================================
    status = check_pass();

  } // Check if the password security is used and the status is zero.


  // ===================================================================
  // Check if the counter is higher than zero.
  // ===================================================================
  if(document.getElementById('countdown').value > 0) {


    // =================================================================
    // Decrease the counter.
    // =================================================================
    new_value = document.getElementById('countdown').value - 1;


    // =================================================================
    // Update the counter display.
    // =================================================================
    document.getElementById('countdown').value = new_value;

  } // Check if the counter is higher than zero.


  // ===================================================================
  // Se the timer for the counter.
  // ===================================================================
  window.setTimeout("countdown()", 1000);

} // Function to countdown the refresh timer.


// =====================================================================
// Function to send the text message if the enter key is pressed.
// =====================================================================
function getkey(event) {


  // ===================================================================
  // Enable the strict mode of JSlint.
  // ===================================================================
  "use strict";


  // ===================================================================
  // Specify the event type.
  // ===================================================================
  event = event || window.event;


  // ===================================================================
  // Check if the keycode is from the enter key.
  // ===================================================================
  if (event.keyCode === 13) {


    // =================================================================
    // Send the text message.
    // =================================================================
    send_input();

  } // Check if the keycode is from the enter key.

} // Function to send the text message if the enter key is pressed.


// =====================================================================
// Function to delete the logfile.
// =====================================================================
function delete_log() {


  // ===================================================================
  // Enable the strict mode of JSlint.
  // ===================================================================
  "use strict";


  // ===================================================================
  // Variable declarations.
  // ===================================================================
  var http = false;
  var status;


  // ===================================================================
  // Check if the password security is used.
  // ===================================================================
  if(use_password === 1) {


    // =================================================================
    // Get the password status.
    // =================================================================
    status = check_pass();


    // =================================================================
    // No password security is used.
    // =================================================================
  } else {


    // =================================================================
    // Specify the password status.
    // =================================================================
    status = 0;

  } // No password security is used.


  // ===================================================================
  // Check if the password status is OK.
  // ===================================================================
  if(status === 1 || use_password === 0) {


    // =================================================================
    // Display an information message.
    // =================================================================
    document.getElementById('output').value = '\n  Delete the chat logfile ...\n';


    // =================================================================
    // Check if the Internet Explorer is used.
    // =================================================================
    if(navigator.appName === "Microsoft Internet Explorer") {


      // ===============================================================
      // Generate a new XML HTTP Request object - Internet Explorer.
      // ===============================================================
      http = new ActiveXObject("Microsoft.XMLHTTP");


      // ===============================================================
      // No Internet Explorer is used.
      // ===============================================================
    } else {


      // ===============================================================
      // Generate a new XML HTTP Request object - Other browsers.
      // ===============================================================
      http = new XMLHttpRequest();

    } // No Internet Explorer is used.


    // =================================================================
    // Abort existing requests.
    // =================================================================
    http.abort();


    // =================================================================
    // Wait for the answer from the host and check if the readyState
    // attribute changes.
    // =================================================================
    http.onreadystatechange=function() {


      // ===============================================================
      // Check if the readyState attribute is set to 4 what means that
      // the request is finished. Check also if the HTTP status is 200
      // what means that the page was available (404 if the page was
      // not found).
      // ===============================================================
      if(http.readyState === 4 && http.status === 200) {


        // =============================================================
        // Check if there is an output.
        // =============================================================
        if(http.responseText !== '') {


          // ===========================================================
          // Insert the chat output into the element with the ID 'output'.
          // ===========================================================
          document.getElementById('output').value = document.getElementById('output').value + http.responseText;


          // ===========================================================
          // Scroll to the bottom of the content.
          // ===========================================================
          document.getElementById('output').scrollTop = document.getElementById('output').scrollHeight;

        } // Check if there is an output.

      } // Check if the readyState attribute is set to 4...

    }; // Wait for the answer from the host and check if the readyState attribute changes.


    // =================================================================
    // Open a connection and make a request to the host.
    // =================================================================
    http.open("GET", location.protocol + "//" + script_dir + "delete_log.php", true);
    http.send(null);

  } // Check if the password status is OK.

} // Function to delete the logfile.


// =====================================================================
// Functions to calculate the MD5 hash of a string.
// =====================================================================
/*
 * JavaScript MD5 1.0.1
 * https://github.com/blueimp/JavaScript-MD5
 *
 * Copyright 2011, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 *
 * Based on
 * A JavaScript implementation of the RSA Data Security, Inc. MD5 Message
 * Digest Algorithm, as defined in RFC 1321.
 * Version 2.2 Copyright (C) Paul Johnston 1999 - 2009
 * Other contributors: Greg Holt, Andrew Kepert, Ydnar, Lostinet
 * Distributed under the BSD License
 * See http://pajhome.org.uk/crypt/md5 for more info.
 */

(function ($) {
    'use strict';

    /*
    * Add integers, wrapping at 2^32. This uses 16-bit operations internally
    * to work around bugs in some JS interpreters.
    */
    function safe_add(x, y) {
        var lsw = (x & 0xFFFF) + (y & 0xFFFF),
            msw = (x >> 16) + (y >> 16) + (lsw >> 16);
        return (msw << 16) | (lsw & 0xFFFF);
    }

    /*
    * Bitwise rotate a 32-bit number to the left.
    */
    function bit_rol(num, cnt) {
        return (num << cnt) | (num >>> (32 - cnt));
    }

    /*
    * These functions implement the four basic operations the algorithm uses.
    */
    function md5_cmn(q, a, b, x, s, t) {
        return safe_add(bit_rol(safe_add(safe_add(a, q), safe_add(x, t)), s), b);
    }
    function md5_ff(a, b, c, d, x, s, t) {
        return md5_cmn((b & c) | ((~b) & d), a, b, x, s, t);
    }
    function md5_gg(a, b, c, d, x, s, t) {
        return md5_cmn((b & d) | (c & (~d)), a, b, x, s, t);
    }
    function md5_hh(a, b, c, d, x, s, t) {
        return md5_cmn(b ^ c ^ d, a, b, x, s, t);
    }
    function md5_ii(a, b, c, d, x, s, t) {
        return md5_cmn(c ^ (b | (~d)), a, b, x, s, t);
    }

    /*
    * Calculate the MD5 of an array of little-endian words, and a bit length.
    */
    function binl_md5(x, len) {
        /* append padding */
        x[len >> 5] |= 0x80 << (len % 32);
        x[(((len + 64) >>> 9) << 4) + 14] = len;

        var i, olda, oldb, oldc, oldd,
            a =  1732584193,
            b = -271733879,
            c = -1732584194,
            d =  271733878;

        for (i = 0; i < x.length; i += 16) {
            olda = a;
            oldb = b;
            oldc = c;
            oldd = d;

            a = md5_ff(a, b, c, d, x[i],       7, -680876936);
            d = md5_ff(d, a, b, c, x[i +  1], 12, -389564586);
            c = md5_ff(c, d, a, b, x[i +  2], 17,  606105819);
            b = md5_ff(b, c, d, a, x[i +  3], 22, -1044525330);
            a = md5_ff(a, b, c, d, x[i +  4],  7, -176418897);
            d = md5_ff(d, a, b, c, x[i +  5], 12,  1200080426);
            c = md5_ff(c, d, a, b, x[i +  6], 17, -1473231341);
            b = md5_ff(b, c, d, a, x[i +  7], 22, -45705983);
            a = md5_ff(a, b, c, d, x[i +  8],  7,  1770035416);
            d = md5_ff(d, a, b, c, x[i +  9], 12, -1958414417);
            c = md5_ff(c, d, a, b, x[i + 10], 17, -42063);
            b = md5_ff(b, c, d, a, x[i + 11], 22, -1990404162);
            a = md5_ff(a, b, c, d, x[i + 12],  7,  1804603682);
            d = md5_ff(d, a, b, c, x[i + 13], 12, -40341101);
            c = md5_ff(c, d, a, b, x[i + 14], 17, -1502002290);
            b = md5_ff(b, c, d, a, x[i + 15], 22,  1236535329);

            a = md5_gg(a, b, c, d, x[i +  1],  5, -165796510);
            d = md5_gg(d, a, b, c, x[i +  6],  9, -1069501632);
            c = md5_gg(c, d, a, b, x[i + 11], 14,  643717713);
            b = md5_gg(b, c, d, a, x[i],      20, -373897302);
            a = md5_gg(a, b, c, d, x[i +  5],  5, -701558691);
            d = md5_gg(d, a, b, c, x[i + 10],  9,  38016083);
            c = md5_gg(c, d, a, b, x[i + 15], 14, -660478335);
            b = md5_gg(b, c, d, a, x[i +  4], 20, -405537848);
            a = md5_gg(a, b, c, d, x[i +  9],  5,  568446438);
            d = md5_gg(d, a, b, c, x[i + 14],  9, -1019803690);
            c = md5_gg(c, d, a, b, x[i +  3], 14, -187363961);
            b = md5_gg(b, c, d, a, x[i +  8], 20,  1163531501);
            a = md5_gg(a, b, c, d, x[i + 13],  5, -1444681467);
            d = md5_gg(d, a, b, c, x[i +  2],  9, -51403784);
            c = md5_gg(c, d, a, b, x[i +  7], 14,  1735328473);
            b = md5_gg(b, c, d, a, x[i + 12], 20, -1926607734);

            a = md5_hh(a, b, c, d, x[i +  5],  4, -378558);
            d = md5_hh(d, a, b, c, x[i +  8], 11, -2022574463);
            c = md5_hh(c, d, a, b, x[i + 11], 16,  1839030562);
            b = md5_hh(b, c, d, a, x[i + 14], 23, -35309556);
            a = md5_hh(a, b, c, d, x[i +  1],  4, -1530992060);
            d = md5_hh(d, a, b, c, x[i +  4], 11,  1272893353);
            c = md5_hh(c, d, a, b, x[i +  7], 16, -155497632);
            b = md5_hh(b, c, d, a, x[i + 10], 23, -1094730640);
            a = md5_hh(a, b, c, d, x[i + 13],  4,  681279174);
            d = md5_hh(d, a, b, c, x[i],      11, -358537222);
            c = md5_hh(c, d, a, b, x[i +  3], 16, -722521979);
            b = md5_hh(b, c, d, a, x[i +  6], 23,  76029189);
            a = md5_hh(a, b, c, d, x[i +  9],  4, -640364487);
            d = md5_hh(d, a, b, c, x[i + 12], 11, -421815835);
            c = md5_hh(c, d, a, b, x[i + 15], 16,  530742520);
            b = md5_hh(b, c, d, a, x[i +  2], 23, -995338651);

            a = md5_ii(a, b, c, d, x[i],       6, -198630844);
            d = md5_ii(d, a, b, c, x[i +  7], 10,  1126891415);
            c = md5_ii(c, d, a, b, x[i + 14], 15, -1416354905);
            b = md5_ii(b, c, d, a, x[i +  5], 21, -57434055);
            a = md5_ii(a, b, c, d, x[i + 12],  6,  1700485571);
            d = md5_ii(d, a, b, c, x[i +  3], 10, -1894986606);
            c = md5_ii(c, d, a, b, x[i + 10], 15, -1051523);
            b = md5_ii(b, c, d, a, x[i +  1], 21, -2054922799);
            a = md5_ii(a, b, c, d, x[i +  8],  6,  1873313359);
            d = md5_ii(d, a, b, c, x[i + 15], 10, -30611744);
            c = md5_ii(c, d, a, b, x[i +  6], 15, -1560198380);
            b = md5_ii(b, c, d, a, x[i + 13], 21,  1309151649);
            a = md5_ii(a, b, c, d, x[i +  4],  6, -145523070);
            d = md5_ii(d, a, b, c, x[i + 11], 10, -1120210379);
            c = md5_ii(c, d, a, b, x[i +  2], 15,  718787259);
            b = md5_ii(b, c, d, a, x[i +  9], 21, -343485551);

            a = safe_add(a, olda);
            b = safe_add(b, oldb);
            c = safe_add(c, oldc);
            d = safe_add(d, oldd);
        }
        return [a, b, c, d];
    }

    /*
    * Convert an array of little-endian words to a string
    */
    function binl2rstr(input) {
        var i,
            output = '';
        for (i = 0; i < input.length * 32; i += 8) {
            output += String.fromCharCode((input[i >> 5] >>> (i % 32)) & 0xFF);
        }
        return output;
    }

    /*
    * Convert a raw string to an array of little-endian words
    * Characters >255 have their high-byte silently ignored.
    */
    function rstr2binl(input) {
        var i,
            output = [];
        output[(input.length >> 2) - 1] = undefined;
        for (i = 0; i < output.length; i += 1) {
            output[i] = 0;
        }
        for (i = 0; i < input.length * 8; i += 8) {
            output[i >> 5] |= (input.charCodeAt(i / 8) & 0xFF) << (i % 32);
        }
        return output;
    }

    /*
    * Calculate the MD5 of a raw string
    */
    function rstr_md5(s) {
        return binl2rstr(binl_md5(rstr2binl(s), s.length * 8));
    }

    /*
    * Calculate the HMAC-MD5, of a key and some data (raw strings)
    */
    function rstr_hmac_md5(key, data) {
        var i,
            bkey = rstr2binl(key),
            ipad = [],
            opad = [],
            hash;
        ipad[15] = undefined;
        opad[15] = undefined;
        if (bkey.length > 16) {
            bkey = binl_md5(bkey, key.length * 8);
        }
        for (i = 0; i < 16; i += 1) {
            ipad[i] = bkey[i] ^ 0x36363636;
            opad[i] = bkey[i] ^ 0x5C5C5C5C;
        }
        hash = binl_md5(ipad.concat(rstr2binl(data)), 512 + data.length * 8);
        return binl2rstr(binl_md5(opad.concat(hash), 512 + 128));
    }

    /*
    * Convert a raw string to a hex string
    */
    function rstr2hex(input) {
        var hex_tab = '0123456789abcdef',
            output = '',
            x,
            i;
        for (i = 0; i < input.length; i += 1) {
            x = input.charCodeAt(i);
            output += hex_tab.charAt((x >>> 4) & 0x0F) +
                hex_tab.charAt(x & 0x0F);
        }
        return output;
    }

    /*
    * Encode a string as utf-8
    */
    function str2rstr_utf8(input) {
        return unescape(encodeURIComponent(input));
    }

    /*
    * Take string arguments and return either raw or hex encoded strings
    */
    function raw_md5(s) {
        return rstr_md5(str2rstr_utf8(s));
    }
    function hex_md5(s) {
        return rstr2hex(raw_md5(s));
    }
    function raw_hmac_md5(k, d) {
        return rstr_hmac_md5(str2rstr_utf8(k), str2rstr_utf8(d));
    }
    function hex_hmac_md5(k, d) {
        return rstr2hex(raw_hmac_md5(k, d));
    }

    function md5(string, key, raw) {
        if (!key) {
            if (!raw) {
                return hex_md5(string);
            }
            return raw_md5(string);
        }
        if (!raw) {
            return hex_hmac_md5(key, string);
        }
        return raw_hmac_md5(key, string);
    }

    if (typeof define === 'function' && define.amd) {
        define(function () {
            return md5;
        });
    } else {
        $.md5 = md5;
    }
}(this));
