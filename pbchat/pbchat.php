<?php


// =====================================================================
// Specify product information.
// =====================================================================
$software_name    = "PBchat";
$software_version = "1.4";
$software_info    = "Simple Chat Application";
$software_website = "pb-soft.com";
$software_company = "PB-Soft";


// =====================================================================
// Check if the server name and server port is set.
// =====================================================================
if (isset($_SERVER['SERVER_NAME']) && isset($_SERVER['SERVER_PORT'])) {


  // ===================================================================
  // Define the actual domain.
  // ===================================================================
  define("DOMAIN", $_SERVER['SERVER_NAME'].":".$_SERVER['SERVER_PORT']);


  // ===================================================================
  // The server name and/or server port is not set.
  // ===================================================================
} else {


  // ===================================================================
  // Define the actual domain.
  // ===================================================================
  define("DOMAIN", $_SERVER['HTTP_HOST']);

} // The server name and/or server port is not set.


// =====================================================================
// Get the actual directory.
// =====================================================================
$path_parts = pathinfo($_SERVER["SCRIPT_NAME"]);


// =====================================================================
// Define the actual directory.
// =====================================================================
define("SCRIPTDIR", DOMAIN.$path_parts['dirname']."/");


// =====================================================================
// Specify the configuration file.
// =====================================================================
$config_file = "config/config.php";


// =====================================================================
// Include the configuration file.
// =====================================================================
include($config_file);


// =====================================================================
// Specify the document type.
// =====================================================================
echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";


// =====================================================================
// Display the html begin.
// =====================================================================
echo "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n";


// =====================================================================
// Display the html header begin.
// =====================================================================
echo "<head>\n";


// =====================================================================
// Specify the content type.
// =====================================================================
echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\" />\n";


// =====================================================================
// Display the page title.
// =====================================================================
echo "<title>";
echo $software_name." ".$software_version." - ".$software_info;
echo "</title>\n";


// =====================================================================
// Display the meta data.
// =====================================================================
echo "<meta name=\"author\" content=\"Patrick Biegel, PB-Soft\" />\n";
echo "<meta name=\"description\" content=\"PBchat is a simple chat application. Check out our website at www.pb-soft.com for more information !\" />\n";
echo "<meta name=\"keywords\" content=\"pb-soft, pb-soft.com, pbchat, web, application, applications, chat, communicate, script, scripts, php\" />\n";


// =====================================================================
// Include the external cascading stylesheet.
// =====================================================================
echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"css/pbchat.css\" />\n";


?>


  <script type="text/javascript">


  // ===================================================================
  // Initialize variables.
  // ===================================================================
  var software_name;
  var use_password;
  var name_padding;
  var info_message;
  var script_dir;
  var saved_pass;
  var show_date;


  // ===================================================================
  // Get variable values from php.
  // ===================================================================
  software_name = "<?php echo $software_name; ?>";
  use_password = <?php echo $use_password; ?>;
  name_padding = <?php echo $name_padding; ?>;
  info_message = <?php echo $info_message; ?>;
  script_dir = "<?php echo SCRIPTDIR; ?>";
  saved_pass = "<?php echo $password; ?>";
  show_date = <?php echo $show_date; ?>;


  </script>


<?php


// =====================================================================
// Include the external javascript file.
// =====================================================================
echo "<script src=\"js/pbchat.js\" type=\"text/javascript\"></script>\n";


// =====================================================================
// HTML head - End.
// =====================================================================
echo "</head>\n";


// =====================================================================
// HTML body - Begin.
// =====================================================================
echo "<body>\n";


// =====================================================================
// DIV element to center the whole page - Begin.
// =====================================================================
echo "<div class=\"center\">\n";


// =====================================================================
// Check if the chat title should be displayed.
// =====================================================================
if ($display_title == 1) {


  // ===================================================================
  // Chat title.
  // ===================================================================
  echo "<div class=\"chat-title\">\n";
  echo $chat_title."\n";
  echo "</div>\n";

} // Check if the chat title should be displayed.


// =====================================================================
// Display the container for the tools-bar - Begin.
// =====================================================================
echo "<div class=\"tools\">\n";


// =====================================================================
// Display the input field for the user name.
// =====================================================================
echo "Username: \n";
echo "<input class=\"name\" name=\"name\" id=\"name\" type=\"text\" size=\"10\" maxlength=\"".($name_padding - 2)."\" />\n";
echo "&nbsp;&nbsp;\n";


// =====================================================================
// Check if the password input field should be displayed.
// =====================================================================
if ($use_password == 1) {


  // ===================================================================
  // Display the input field for the user name.
  // ===================================================================
  echo "Password: \n";
  echo "<input class=\"password\" name=\"password\" id=\"password\" type=\"password\" size=\"8\" maxlength=\"20\" />\n";
  echo "<span id=\"status\"></span>\n";
  echo "&nbsp;&nbsp;\n";

} // Check if the password input field should be displayed.


// =====================================================================
// Display the input field for the refresh time.
// =====================================================================
echo "Refresh: \n";
echo "<input class=\"time\" name=\"time\" id=\"time\" type=\"text\" size=\"2\" maxlength=\"3\" value=\"".$default_refresh."\" />\n";
echo "&nbsp;&nbsp;\n";


// =====================================================================
// Display the remaining time until the next refresh.
// =====================================================================
echo "Remaining: \n";
echo "<input class=\"countdown\" name=\"countdown\" id=\"countdown\" type=\"text\" size=\"2\" maxlength=\"3\" readonly=\"readonly\" value=\"".$default_refresh."\" />\n";
echo "&nbsp;&nbsp;\n";


// =====================================================================
// Display the container for the tools-bar - End.
// =====================================================================
echo "</div>\n";


// =====================================================================
// Display the textarea for the chat output.
// =====================================================================
echo "<textarea class=\"output\" name=\"output\" id=\"output\" cols=\"10\" rows=\"1\" readonly=\"readonly\"></textarea>\n";
echo "<br />\n";


// =====================================================================
// Display the textarea for the chat input.
// =====================================================================
echo "<textarea class=\"input\" name=\"input\" id=\"input\" cols=\"10\" rows=\"1\" onkeydown=\"getkey(event);\"></textarea>\n";
echo "<br />\n";


// =====================================================================
// Display the link to view the logfile.
// =====================================================================
echo "<span class=\"button\">\n";
echo "<a href=\"http://".SCRIPTDIR.$logfile."\" onclick=\"window.open(this.href, 'Logfile').focus(); return false;\">";
echo "Show Logfile";
echo "</a>\n";
echo "</span>\n";
echo "&nbsp;&nbsp;\n";


// =====================================================================
// Check if the link to delete the logfile should be displayed.
// =====================================================================
if ($delete_link == 1) {


  // ===================================================================
  // Initialize the admin flag.
  // ===================================================================
  $admin_flag = 0;


  // ===================================================================
  // Get the IP address parts.
  // ===================================================================
  $ip_parts = explode(".", $_SERVER["REMOTE_ADDR"]);


  // ===================================================================
  // Check if the actual IP address is in the admin array - Test 1.
  // ===================================================================
  if (in_array($ip_parts[0], $admin_ip)) {


    // =================================================================
    // Enable the admin flag.
    // =================================================================
    $admin_flag = 1;


    // =================================================================
    // Check if the actual IP address is in the admin array - Test 2.
    // =================================================================
  } elseif (in_array($ip_parts[0].".".$ip_parts[1], $admin_ip)) {


    // =================================================================
    // Enable the admin flag.
    // =================================================================
    $admin_flag = 1;


    // =================================================================
    // Check if the actual IP address is in the admin array - Test 3.
    // =================================================================
  } elseif (in_array($ip_parts[0].".".$ip_parts[1].".".$ip_parts[2], $admin_ip)) {


    // =================================================================
    // Enable the admin flag.
    // =================================================================
    $admin_flag = 1;


    // =================================================================
    // Check if the actual IP address is in the admin array - Test 4.
    // =================================================================
  } elseif (in_array($ip_parts[0].".".$ip_parts[1].".".$ip_parts[2].".".$ip_parts[3], $admin_ip)) {


    // =================================================================
    // Enable the admin flag.
    // =================================================================
    $admin_flag = 1;

  } // Check if the actual IP address is in the admin array - Test 4.


  // ===================================================================
  // Check if the actual IP address is in the admin array.
  // ===================================================================
  if ($admin_flag == 1) {


    // =================================================================
    // Display the link to delete the logfile.
    // =================================================================
    echo "<span class=\"button\">\n";
    echo "<a onclick=\"delete_log()\">";
    echo "Delete Logfile";
    echo "</a>\n";
    echo "</span>\n";
    echo "&nbsp;&nbsp;\n";

  } // Check if the actual IP address is in the admin array.

} // Check if the link to delete the logfile should be displayed.


// =====================================================================
// Check if the password security is used.
// =====================================================================
if ($use_password == 1) {


  // ===================================================================
  // Display the button to reset the password input field.
  // ===================================================================
  echo "<span class=\"button\">\n";
  echo "<a onclick=\"document.getElementById('password').value = '';\">";
  echo "Reset Password";
  echo "</a>\n";
  echo "</span>\n";
  echo "&nbsp;&nbsp;\n";

} // Check if the password security is used.


// =====================================================================
// Display the button to reset the input screen.
// =====================================================================
echo "<span class=\"button\">\n";
echo "<a onclick=\"document.getElementById('input').value = '';\">";
echo "Reset Input";
echo "</a>\n";
echo "</span>\n";
echo "&nbsp;&nbsp;\n";


// =====================================================================
// Display the button to reset the output screen.
// =====================================================================
echo "<span class=\"button\">\n";
echo "<a onclick=\"document.getElementById('output').value = '';\">";
echo "Reset Output";
echo "</a>\n";
echo "</span>\n";
echo "&nbsp;&nbsp;\n";


// =====================================================================
// Display the send button.
// =====================================================================
echo "<span class=\"button\">\n";
echo "<a onclick=\"send_input()\">";
echo "Send Message";
echo "</a>\n";
echo "</span>\n";


// =====================================================================
// Display the copyright.
// =====================================================================
echo "<div class=\"copyright\">\n";
echo "Copyright &copy; ".date("Y")." by <a class=\"copyright\" href=\"http://".$software_website."\" onclick=\"window.open(this.href, 'Logfile').focus(); return false;\">".$software_company."</a>\n";
echo " | IP address: ".$_SERVER["REMOTE_ADDR"];
echo "</div>\n";


// =====================================================================
// DIV element to center the whole page - End.
// =====================================================================
echo "</div>\n";


?>


<script type="text/javascript">


  // ===================================================================
  // Display the start information message.
  // ===================================================================
  display_info();


  // ===================================================================
  // Start the countdown.
  // ===================================================================
  countdown();


  // ===================================================================
  // Start the chat actualization.
  // ===================================================================
  actualize_chat();


</script>


<?php


// =====================================================================
// HTML body - End.
// =====================================================================
echo "</body>\n";


// =====================================================================
// HTML page - End.
// =====================================================================
echo "</html>\n";
